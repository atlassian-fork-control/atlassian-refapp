# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

- Added Client-Side Extensions runtime.
- The home and admin pages are now built in React.
- The extensions to these pages are now client-side extensions.

## [5.1.3]

## [5.1.2]

## [5.1.1]

## [5.0.0]

## [5.0.5] - 2019-12-05

### Changed
- REFAPP-509 Added AMD shim for jQuery

### Fixed
- BSP-696 Bumped bugfix version of platform POM, to 5.0.19, to fix semver violations in 5.0.12

## [5.0.4] - 2019-08-27

### Fixed
- BSP-527 `web-item-plugin` now exposes `DynamicWebInterfaceManager` as an OSGi service

## [5.0.3] - 2019-08-05

### Changed
- BSP-504 Bumped minor version of AUI, to 8.3.4
- BSP-504 Bumped minor version of jslibs, to 1.4.1

### Fixed
- BSP-504 Bumped bugfix version of platform POM, to 5.0.10
- BSP-504 Bumped bugfix version of Spring Scanner, to 2.1.10
- BSP-504 Bumped bugfix version of UPM, to 4.0.4
- BSP-504 Fixed `$.browser` by adding `jquery` plugin to resource batch

## [5.0.2] - 2019-04-12

### Changed
- Exposed `SchedulerHistoryService` as an OSGi service
- BSP-370 Bumped bugfix version of platform POM, to 5.0.2

## [5.0.1] - 2019-04-02

### Changed
- BSP-367 Bumped bugfix version of platform POM, to 5.0.1

## [5.0.0] - 2019-01-09

### Changed
- REFAPP-505 Server Platform 5.0 compatible, supports Java 11

## [3.3.11] - 2018-05-09

### Fixed
- [KYAK-66]: Fixed `atlassian-refapp-pageobjects` compatibility with new Ubuntu 16.04 build agents

[3.3.11]: https://bitbucket.org/atlassian/atlassian-refapp/compare/atlassian-refapp-parent-3.3.11%0Datlassian-refapp-parent-3.3.9
[KYAK-66]: https://bulldog.internal.atlassian.com/browse/KYAK-66
