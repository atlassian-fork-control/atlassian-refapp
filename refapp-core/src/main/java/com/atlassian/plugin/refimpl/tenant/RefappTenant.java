package com.atlassian.plugin.refimpl.tenant;

import com.atlassian.tenancy.api.Tenant;

/**
 * This is a simple implementation of the Tenant, as part of the Tenancy API.
 */
public class RefappTenant implements Tenant {
    private String tenantId;

    public RefappTenant(String tenantId) {
        this.tenantId = tenantId;
    }

    protected String getTenantID() {
        return tenantId;
    }

    @Override
    public String name() {
        return "RefApp tenant";
    }
}
