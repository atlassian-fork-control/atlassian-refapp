package ctk.com.atlassian.refapp;

import com.atlassian.functest.client.RemoteTestRunner;
import it.com.atlassian.refapp.RefappTestURLs;
import org.junit.runner.RunWith;

@RunWith(RemoteTestRunner.class)
public class CtkTestRunner {

    @RemoteTestRunner.BaseURL
    public static String base() {
        return RefappTestURLs.BASEURL;
    }

    @RemoteTestRunner.Group(name = "standard-test")
    public void standardTest() {
    }
}
