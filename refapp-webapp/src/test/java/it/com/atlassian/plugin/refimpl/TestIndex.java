package it.com.atlassian.plugin.refimpl;

import com.atlassian.webdriver.refapp.page.RefappPluginIndexPage;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import org.junit.Test;

import java.util.Collections;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class TestIndex extends AbstractRefappTestCase {
    @Test
    public void testIndex() {
        RefappPluginIndexPage pluginIndexPage = PRODUCT.visit(RefappPluginIndexPage.class);
        Set<String> pluginKeys = pluginIndexPage.getPluginKeys();
        pluginKeys.contains("com.atlassian.plugin.osgi.bridge");


        Set<RefappPluginIndexPage.Bundle> bundles = pluginIndexPage.getBundles();

        assertFalse("Plugins should be present", bundles.isEmpty());

        assertEquals("none of the plugins should be just 'Installed'",
                Collections.emptyList(),
                ImmutableList.copyOf(
                        Iterables.transform(Iterables.filter(bundles, IS_INSTALLED), GET_BUNDLE_DESCRIPTION)
                ));
    }

    private static Predicate<RefappPluginIndexPage.Bundle> IS_INSTALLED = new Predicate<RefappPluginIndexPage.Bundle>() {
        public boolean apply(RefappPluginIndexPage.Bundle bundle) {
            return bundle.getState().equals("Installed");
        }
    };

    private static Function<RefappPluginIndexPage.Bundle, String> GET_BUNDLE_DESCRIPTION = new Function<RefappPluginIndexPage.Bundle, String>() {
        public String apply(RefappPluginIndexPage.Bundle bundle) {
            return bundle.getSymbolicName() + " " + bundle.getVersion();
        }
    };
}
