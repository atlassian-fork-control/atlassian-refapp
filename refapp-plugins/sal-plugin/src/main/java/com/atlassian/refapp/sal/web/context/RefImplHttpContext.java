package com.atlassian.refapp.sal.web.context;

import com.atlassian.plugin.refimpl.saldeps.ServletContextThreadLocal;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.sal.api.web.context.HttpContext;

import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@ExportAsService
@Named("httpContext")
public class RefImplHttpContext implements HttpContext {
    @Override
    public HttpServletRequest getRequest() {
        return ServletContextThreadLocal.getRequest();
    }

    @Override
    public HttpServletResponse getResponse() {
        return ServletContextThreadLocal.getResponse();
    }

    @Override
    public HttpSession getSession(boolean create) {
        final HttpServletRequest request = getRequest();
        if (request == null)
            return null;
        return request.getSession(create);
    }
}
