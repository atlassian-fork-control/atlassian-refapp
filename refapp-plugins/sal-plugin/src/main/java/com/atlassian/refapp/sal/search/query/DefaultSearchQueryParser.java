package com.atlassian.refapp.sal.search.query;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.sal.api.search.query.SearchQuery;
import com.atlassian.sal.api.search.query.SearchQueryParser;

import javax.inject.Named;


/**
 * Factory for creating SearchQueries
 */
@ExportAsService
@Named("searchQueryParser")
public class DefaultSearchQueryParser implements SearchQueryParser {
    public SearchQuery parse(String query) {
        return new DefaultSearchQuery(query);
    }
}
