package com.atlassian.refapp.sal.license;

import com.atlassian.extras.api.LicenseException;
import com.atlassian.extras.api.Product;
import com.atlassian.extras.api.ProductLicense;
import com.atlassian.extras.common.util.LicenseProperties;
import com.atlassian.extras.core.AbstractProductLicenseFactory;
import com.atlassian.extras.core.DefaultProductLicense;

/**
 * RefApp analogues of the per product constants in {@link Product} and associated classes.
 *
 * @since 3.0.0
 */
public class RefappProduct {
    private static final String REFAPP_NS = "refapp";

    public static Product REFAPP = new Product("RefImpl", REFAPP_NS);

    private static class RefappProductLicense extends DefaultProductLicense {
        public RefappProductLicense(final Product product, final LicenseProperties licenseProperties) {
            super(product, licenseProperties);
        }
    }

    static class RefappProductLicenseFactory extends AbstractProductLicenseFactory {
        @Override
        protected ProductLicense getLicenseInternal(final Product product, final LicenseProperties licenseProperties) {
            if (REFAPP.equals(product)) {
                return new RefappProductLicense(product, licenseProperties);
            } else {
                throw new LicenseException("Cannot create license for " + product);
            }
        }
    }
}
