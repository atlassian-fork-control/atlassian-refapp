const path = require('path');
const process = require('process');
const WrmPlugin = require('atlassian-webresource-webpack-plugin');
const CsePlugin = require('@atlassian/clientside-extensions-webpack-plugin');

const mode = process.env.NODE_ENV !== 'development' ? 'production' : 'development';

const projectRootDir = path.resolve(__dirname);
const frontendDir = path.join(projectRootDir, 'src/main/frontend');
const outputDir = path.join(projectRootDir, 'target/classes');

const wrmPlugin = new WrmPlugin({
    pluginKey: 'com.atlassian.refapp.clientside-extensions',
    xmlDescriptors: path.join(outputDir, 'META-INF/plugin-descriptors/webpacked-descriptors.xml')
});

const extensionsPlugin = new CsePlugin({
    cwd: frontendDir,
    pattern: '**/*.js'
});

module.exports = {
    mode,
    entry: {
        ...extensionsPlugin.generateEntrypoints()
    },
    output: {
        path: path.resolve(projectRootDir, 'target/classes'),
    },
    plugins: [wrmPlugin, extensionsPlugin],
};
