package com.atlassian.refapp.auth.internal;

import javax.annotation.Nonnull;

/**
 * @since 3.0
 */
public enum RefappPermissions {

    USER("users"),
    ADMIN("administrators"),
    SYSADMIN("system_administrators");

    private final String groupName;

    RefappPermissions(String groupName) {
        this.groupName = groupName;
    }

    @Nonnull
    public String groupName() {
        return groupName;
    }
}
