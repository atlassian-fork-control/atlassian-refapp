package com.atlassian.refapp.auth.internal;

import com.atlassian.seraph.auth.AuthenticationContext;
import com.atlassian.user.EntityException;
import com.atlassian.user.Group;
import com.atlassian.user.GroupManager;
import com.atlassian.user.User;
import com.atlassian.user.UserManager;

import java.security.Principal;

public class UserContextHelper {
    private final AuthenticationContext authenticationContext;
    private final UserManager userManager;
    private final GroupManager groupManager;

    public UserContextHelper(AuthenticationContext authenticationContext, UserManager userManager,
                             GroupManager groupManager) {
        this.authenticationContext = authenticationContext;
        this.userManager = userManager;
        this.groupManager = groupManager;
    }

    public User getRemoteUser() {
        Principal principal = authenticationContext.getUser();
        if (principal == null) {
            return null;
        }
        try {
            return userManager.getUser(principal.getName());
        } catch (EntityException ee) {
            return null;
        }
    }

    public boolean isRemoteUserAdministrator() {
        return isRemoteUserRole("administrators");
    }

    public boolean isRemoteUserSystemAdministrator() {
        return isRemoteUserRole("system_administrators");
    }

    private boolean isRemoteUserRole(String role) {
        User user = getRemoteUser();
        if (user == null) {
            return false;
        }
        try {
            Group group = groupManager.getGroup(role);
            if (group == null) {
                return false;
            }
            return groupManager.hasMembership(group, user);
        } catch (EntityException ee) {
            return false;
        }
    }

}
