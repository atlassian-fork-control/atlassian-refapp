package com.atlassian.refapp.auth.internal.rest;

import com.atlassian.refapp.auth.internal.RefappPermissions;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize
public class RestUser {
    @JsonProperty
    String name;
    @JsonProperty
    String fullName;
    @JsonProperty
    String email;
    @JsonProperty
    RefappPermissions permissionLevel;
}
