package com.atlassian.refapp.auth.internal;

import com.atlassian.refapp.auth.external.WebSudoSessionManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DefaultWebSudoSessionManagerTest {
    private static final long CURRENT_MILLIS = 1234L;
    private static final String SESS_KEY = DefaultWebSudoSessionManager.class.getName() + "-session";

    private WebSudoSessionManager webSudoSessionManager;

    @Mock
    private HttpServletRequest httpServletRequest;

    @Mock
    private HttpSession session;

    @Before
    public void setUp() {

        when(httpServletRequest.getSession(true)).thenReturn(session);
        when(httpServletRequest.getSession(false)).thenReturn(session);

        webSudoSessionManager = new DefaultWebSudoSessionManager() {
            @Override
            long currentTimeMillis() {
                return CURRENT_MILLIS;
            }
        };
    }

    @After
    public void tearDown() {
        webSudoSessionManager = null;
    }

    @Test
    public void isWebSudoSession() {
        assertThat(webSudoSessionManager.isWebSudoSession(httpServletRequest), is(false));
    }

    @Test
    public void isWebSudoSessionExpired() {
        when(session.getAttribute(SESS_KEY)).thenReturn(CURRENT_MILLIS - 2);
        assertThat(webSudoSessionManager.isWebSudoSession(httpServletRequest), is(true));
    }

    @Test
    public void isWebSudoSessionTrue() {
        webSudoSessionManager = new DefaultWebSudoSessionManager() {
            @Override
            long currentTimeMillis() {
                return CURRENT_MILLIS + TimeUnit.SECONDS.toMillis(12 * 60);
            }
        };
        when(session.getAttribute(SESS_KEY)).thenReturn(CURRENT_MILLIS);
        assertThat(webSudoSessionManager.isWebSudoSession(httpServletRequest), is(false));
    }

    @Test
    public void createWebSudoSession() {
        webSudoSessionManager.createWebSudoSession(httpServletRequest);
        verify(session).setAttribute(SESS_KEY, CURRENT_MILLIS);
    }

    @Test
    public void removeWebSudoSession() {
        webSudoSessionManager.removeWebSudoSession(httpServletRequest);
        verify(session).removeAttribute(SESS_KEY);
    }

    @Test
    public void disableWebSudoSession() {
        System.setProperty(DefaultWebSudoSessionManager.WEB_SUDO_CHECKING_DISABLED_PROPERTY,"true");
        assertThat(webSudoSessionManager.isWebSudoSession(httpServletRequest), is(true));
        System.setProperty(DefaultWebSudoSessionManager.WEB_SUDO_CHECKING_DISABLED_PROPERTY,"false");
    }
}
