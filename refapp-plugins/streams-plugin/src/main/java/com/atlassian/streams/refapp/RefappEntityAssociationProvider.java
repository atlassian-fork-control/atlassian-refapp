package com.atlassian.streams.refapp;

import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.spi.EntityIdentifier;
import com.atlassian.streams.spi.StreamsEntityAssociationProvider;

import java.net.URI;

import static com.atlassian.streams.api.common.Option.none;
import static java.util.Collections.emptyList;

public class RefappEntityAssociationProvider implements StreamsEntityAssociationProvider {
    public Iterable<EntityIdentifier> getEntityIdentifiers(URI uri) {
        return emptyList();
    }

    public Option<URI> getEntityURI(EntityIdentifier entityIdentifier) {
        return none();
    }

    public Option<String> getFilterKey(EntityIdentifier entityIdentifier) {
        return none();
    }

    public Option<Boolean> getCurrentUserViewPermission(EntityIdentifier entityIdentifier) {
        return none();
    }

    public Option<Boolean> getCurrentUserEditPermission(EntityIdentifier entityIdentifier) {
        return none();
    }
}
