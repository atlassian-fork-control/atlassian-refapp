package com.atlassian.streams.refapp.api;

import com.atlassian.streams.api.ActivityRequest;
import com.atlassian.streams.api.StreamsEntry;

import java.net.URI;

/**
 * Provide backdoor to test create/delete streams activity entries.
 */
public interface StreamsActivityManager {
    /**
     * Get the activities, with filter applied.
     *
     * @param activityRequest the activities request.
     * @return a non-null collection of entries.
     */
    public Iterable<StreamsEntry> getEntries(final ActivityRequest activityRequest);

    /**
     * Add an activities entry to the in-memory buffer. If the maximum size of the buffer reach, the oldest entry is
     * removed.
     *
     * @param entryRequest the entry information to add.
     */
    public void addEntry(StreamsEntryRequest entryRequest);

    /**
     * Remove an activity entry.
     *
     * @param id the entry to remove.
     * @return true if the id is found and removed, otherwise false.
     */
    public boolean removeEntry(final int id);

    /**
     * Get entry by id.
     *
     * @param id id of the entry.
     * @return entry, null if not found.
     */
    public StreamsEntry getEntry(final int id);

    /**
     * Build the URI to access entry detail from integer id.
     *
     * @param id id.
     * @return URI.
     */
    public URI buildUriId(final int id);
}
