var reload = false;
var reloadInterval = 10000; //10s

AJS.toInit(function()
{
    var loadStreams = function()
    {
        if (reload) 
        {
            removeOldData();
        }
        
        AJS.$.ajax(
        {
            type: 'get',
            url: activityStreamsBaseUrl,
            global: false,
            dataType: (AJS.$.browser.msie) ? 'text' : 'xml',
            success: function (data, status)
            {
                if (status !== 'parsererror')
                {
                    var feed = jAtom(data);
                    var items = feed.items;
                    var itemsLength = items.length;
                    for (var i = 0; i < itemsLength; i++)
                    {
                        var item = items[i];
                        addNewRow(item);
                    }
                }
                reload = true;
            }
        });
    }
    
    if (!reload)
    {
        loadStreams();
    }
    
    setInterval(loadStreams, 10000); //10s
});

function addNewRow(item)
{
    var newRow = "<tr>" +
        "<td>" + item.id + "</td>" +
        "<td>" + item.title + "</td>" +
        "</tr>";
    AJS.$('#streamsTable > tbody > tr:last').after(AJS.$(newRow));
}

function removeOldData()
{
    while (AJS.$("#streamsTable > tbody > tr").length != 1)
    {
        AJS.$("#streamsTable > tbody > tr:last").remove();
    }
}