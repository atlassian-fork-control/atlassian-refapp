package com.atlassian.refapp.webitem;

import com.atlassian.plugin.web.api.DynamicWebInterfaceManager;
import com.atlassian.plugin.web.descriptors.DefaultWebItemModuleDescriptor;

public class RefAppWebItemModuleDescriptor extends DefaultWebItemModuleDescriptor {
    public RefAppWebItemModuleDescriptor(DynamicWebInterfaceManager webInterfaceManager) {
        super(webInterfaceManager);
    }
}
