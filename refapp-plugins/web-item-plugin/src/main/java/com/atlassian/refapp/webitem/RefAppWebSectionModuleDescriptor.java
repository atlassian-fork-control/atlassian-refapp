package com.atlassian.refapp.webitem;

import com.atlassian.plugin.web.api.DynamicWebInterfaceManager;
import com.atlassian.plugin.web.descriptors.DefaultWebSectionModuleDescriptor;

public class RefAppWebSectionModuleDescriptor extends DefaultWebSectionModuleDescriptor {
    public RefAppWebSectionModuleDescriptor(DynamicWebInterfaceManager webInterfaceManager) {
        super(webInterfaceManager);
    }
}
