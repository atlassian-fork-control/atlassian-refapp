import React, { FC } from 'react';
import { useExtensionsAll } from '@atlassian/clientside-extensions-components';

const clientSideExtensionExample = ``;

const webItemXmlExample = `
<web-item key="myLink" section="index.links" weight="40" application="refapp">
    <label key="My Link Name or I18n Key">
    <link linkId="myLinkId">/relative/or/absolute/path/to/my/link</link>
</web-item>
`;

const context = null as object;
const schema = {
    type: 'object',
    properties: {
        type: {
            type: 'string',
            enum: ['link'],
        },
        label: {
            type: 'string',
            description: 'Text to render for the link',
        },
        url: {
            type: 'string',
            description: 'Where to go when the user clicks the link',
        },
        onAction: {
            typeof: 'function',
            description: 'Callback will be run when user clicks the link',
        },
    },
};

interface SimpleLink {
    url?: string;
    key: string;
    label: string;
}

const loadingIndicator = (
    <p>
        Loading <code>index.links</code>...
    </p>
);

const WebItemSection: FC = () => {
    const [descriptors, olderDescriptors, loading] = useExtensionsAll('index.links', context, {
        schema,
    });

    let unorderedListOfItems = (
        <p>
            No items were registered to <code>index.links</code>.
        </p>
    );

    if (!loading) {
        const links: SimpleLink[] = [...descriptors, ...olderDescriptors].map(
            ({ key, attributes }) => {
                return {
                    key,
                    url: attributes.url as string,
                    label: attributes.label as string,
                };
            }
        );
        if (links.length) {
            unorderedListOfItems = (
                <ul>
                    {links.map(attrs => (
                        <li key={attrs.key}>
                            <a href={attrs.url}>{attrs.label}</a>
                        </li>
                    ))}
                </ul>
            );
        }
    }

    return (
        <>
            <h2> Web Items</h2>

            <div id="js-index-webitems">
                {loading && loadingIndicator}
                {!loading && unorderedListOfItems}
            </div>

            <details>
                <summary>Wondering how the above links got there?</summary>

                <p>
                    They&apos;re all <dfn>web items</dfn>. These ones are added to the{' '}
                    <code>index.links</code> extension point. You can add another link to this
                    extension point through your plugin in two ways:
                </p>
                <ul>
                    <li>
                        <p>Using client-side extensions:</p>
                        <code>
                            <pre>{clientSideExtensionExample}</pre>
                        </code>
                    </li>
                    <li>
                        <p>Using XML descriptors:</p>
                        <code>
                            <pre>{webItemXmlExample}</pre>
                        </code>
                    </li>
                </ul>
            </details>

            <p>
                Other places you can add links in the refapp include{' '}
                <code>system.admin/general</code>,<code>header.links</code>, and{' '}
                <code>footer.links</code>.
            </p>
        </>
    );
};

export default WebItemSection;
