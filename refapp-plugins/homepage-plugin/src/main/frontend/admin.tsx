import React from 'react';
import ReactDOM from 'react-dom';
import whenDomReady from 'when-dom-ready';
import { hot } from 'react-hot-loader'; // eslint-disable-line
import App from './pages/admin/AdminPage';

whenDomReady().then(function example() {
    const container = document.getElementById('refapp-adminpage-ui');
    // eslint-disable-next-line no-undef
    const MyApp = hot(module)(() => <App />);
    ReactDOM.render(<MyApp />, container);
});
