package com.atlassian.refapp.api;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Optional;

/**
 * Provides a database connection for use by core and plugins.
 * <p>
 * An H2 server (internal) database will be provided, unless the user specifies an external database via system properties:
 * <p>
 * <code>refapp.jdbc.external</code> set this to true to use an external database
 * <code>refapp.jdbc.driver.class.name</code> {@link java.sql.Driver} implementation to use
 * <code>refapp.jdbc.app.url</code> mandatory jdbc url
 * <code>refapp.jdbc.app.schema</code> optional jdbc schema - don't specify if you don't use a schema e.g. mysql, however must still be specified for all other databases; don't rely on the default schema
 * <code>refapp.jdbc.app.user</code> mandatory jdbc user
 * <code>refapp.jdbc.app.pass</code> mandatory jdbc password
 * <code>refapp.jdbc.app.val.query</code> optional validation query for drivers that don't support {@link Connection#isValid(int)}
 */
public interface ConnectionProvider {

    /**
     * Provides a connection from a pool.
     * <p>
     * Users may close the connection.
     * Users may create transactions within the connection.
     *
     * @return valid connection
     * @throws java.sql.SQLException on any failure to provide connection
     */
    Connection connection() throws SQLException;

    /**
     * Provides the {@link DataSource} used to source any database connections
     *
     * @return valid DataSource
     * @since 3.2
     */
    DataSource dataSource();

    /**
     * Returns the configured schema name (if any)
     *
     * @return schema name, if there is one
     * @since 3.2
     */
    Optional<String> schema();

    /**
     * Returns the configured database driver class name
     *
     * @return fully qualified driver class name
     * @since 3.2
     */
    String driverClassName();
}
